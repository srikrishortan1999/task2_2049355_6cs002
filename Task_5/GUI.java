package baseball;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class GUI {
	   public static void main(String[] args) {
		      JFrame f = new JFrame("The Baseball League");
		      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		      f.setSize(300, 250);
		      Container contentPane = f.getContentPane();
		      contentPane.setLayout(new BorderLayout());
		      
		      JLabel l= new JLabel("The Baseball League");
		      l.setHorizontalAlignment(JLabel.CENTER);
		      contentPane.add(l, BorderLayout.NORTH);
		      
		      JPanel bp = new JPanel();
		      bp.setLayout(new GridLayout(5, 200));
		      contentPane.add(bp );
		      
		      JButton b1 = new JButton("Baseball - 01: list of integers with stream");
		      bp .add(b1);
		      baseball_01.addActionListener(new ActionListener() {
		    	  public void actionPerformed(ActionEvent e) {
		    		  baseball_01.main(null);
		    	  }
		      });
		      
		      JButton b2 = new JButton("Baseball - 02: Parallel Streaming");
		      bp .add(b2);
		      baseball_02.addActionListener(new ActionListener() {
		    	  public void actionPerformed(ActionEvent e) {
		    		  baseball_01.main(null);
		    	  }
		      });
		      
		      JButton b3 = new JButton("Baseball - 03: Max, Min, Reduce, Map and Collect");
		      bp .add(b3);
		      baseball_03.addActionListener(new ActionListener() {
		    	  public void actionPerformed(ActionEvent e) {
		    		  baseball_01.main(null);
		    	  }
		      });
		      
		      JButton b4 = new JButton("Baseball - 04: Filter ");
		      bp .add(b4);
		      baseball_04.addActionListener(new ActionListener() {
		    	  public void actionPerformed(ActionEvent e) {
		    		  baseball_01.main(null);
		    	  }
		      });
		      
		      JButton b5 = new JButton("Baseball - 05: Sorting");
		      bp .add(b5);
		      baseball_05.addActionListener(new ActionListener() {
		    	  public void actionPerformed(ActionEvent e) {
		    		  baseball_01.main(null);
		    	  }
		      });
		      
		      JButton eb = new JButton("Exit");
		      eb.addActionListener(e -> System.exit(0));
		      contentPane.add(eb, BorderLayout.SOUTH);
		      f.setVisible(true);
	   }

}

