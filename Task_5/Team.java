package baseball;

public class Team implements Comparable<Team> {
  private int rank;
  private String team_name;
  private String division;
  private String league;
  private int wins;
  private int losses;
  private int winning_percentage;
  private int games_behind;
  private int wildcard_gamesbehind;
  private int runs_scored;
  private int runs_allowed;
  private int runs_differential;

  public Team(int rank, String team_name, String division,String league, 
	  int wins, int losses,int winning_percentage, int games_behind, 
	  int wildcard_gamesbehind,int runs_scored, int runs_differential,int runs_allowed) {  
    this.rank = rank;
    this.team_name = team_name;
    this.division = division;
    this.league = league;
    this.wins = wins;
    this.losses = losses;
    this.winning_percentage = winning_percentage;
    this.games_behind = games_behind;
    this.wildcard_gamesbehind = wildcard_gamesbehind;
    this.runs_scored = runs_scored;
    this.runs_allowed = runs_allowed;
    this.runs_differential = runs_differential;  
  }

  public String toString() {
    return String.format("%-3d%-20s%10d%10d%10d", rank, team_name,winning_percentage,wildcard_gamesbehind,
    		runs_differential);
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int Rank) {
    this.rank = Rank;
  }

  public String getTeam_Name() {
    return team_name;
  }

  public void setTeam_Name(String team_name) {
    this.team_name = team_name;
  }
  
  public String getDivision() {
    return division;
  }

  public void setDivision(String division) {
    this.division = division;
  }
  
  public String getLeague() {
	    return league;
	  }
  
 public void setLeague(String league) {
	    this.league = league;
	  }

  public int getWins() {
    return wins;
  }

  public void setWins(int wins) {
    this.wins = wins;
  }

  public int getLosses() {
    return losses;
  }

  public void setLosses(int losses) {
    this.losses = losses;
  }

  public int getWinning_Percentage() {
    return winning_percentage;
  }

  public void setWinning_Percentage(int winning_percentage) {
    this.winning_percentage = winning_percentage;
  }

  public int getGamesBehind() {
    return games_behind;
  }

  public void setGamesBehind(int games_behind) {
    this.games_behind= games_behind;
  }

  public int getWidcardGamesbehind() {
    return wildcard_gamesbehind;
  }

  public void setWildcardGamesbehind(int wildcard_gamesbehind) {
    this.wildcard_gamesbehind = wildcard_gamesbehind;
  }

  public int getRunsScored() {
    return runs_scored;
  }

  public void setRunsScored(int runs_scored) {
    this.runs_scored = runs_scored;
  }

  public int getRuns_Allowed() {
    return runs_allowed;
  }

  public void setRuns_Allowed(int runs_allowed) {
    this.runs_allowed = runs_allowed;
  }

  public int getRuns_Differential() {
    return runs_differential;
  }

  public void setRuns_Differential(int runs_differential) {
    this.runs_differential = runs_differential;
  }
  
  public int compareTo(Team team) {
    return ((Integer) winning_percentage).compareTo(team.winning_percentage);
  }
}
