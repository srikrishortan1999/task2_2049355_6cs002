package baseball;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class baseball_03 {
  public static void main(String[] args) {
    List<Team> table = Arrays.asList(
        new Team(1, "New York","East","American League", 99, 63, 51, 0, 0, 807,567,+240),
    	new Team(2, "Toronto","East","American League", 92, 73, 41, 7, 6, 451,487,-30),
    	new Team(3, "Cleveland","Central","American League", 49, 65, 47, 5, 12, 340,420,-80),
    	new Team(4, "Minnesota","Central","American League", 79, 43, 53, 14, 7, 457,327,+100),
    	new Team(5, "Houston","West","American League", 41, 52, 72, 2, 2, 478,788,-257),
    	new Team(6, "Seattle","West","American League", 27, 44, 32, 8, 4,880,740,+110),
    	new Team(7, "Atlanta","East","National League", 45, 57, 80, 7, 0, 234,320,-60),
    	new Team(8, "Miami","East","National League", 35, 42, 52, 2, 6, 447,337,+150),
    	new Team(9, "Milwaukee","Central","National League", 42, 47, 52, 5, 5, 927,347,+420),
    	new Team(10, "Pittsburgh","Central","National League", 70, 60, 41, 4, 7, 320,176,+144),
    	new Team(11, "Arizona","West","National League", 107, 41, 50, 1, 1, 527,206,+180),
    	new Team(12, "Colorado","West","National League", 82, 12, 90, 3, 10, 657,787,-210));
    OptionalInt max = table.stream().mapToInt(Team::getWins).max();
    if (max.isPresent()) {
      System.out.printf("The Highest Number of Wins is %d\n", max.getAsInt());
    } else {
      System.out.println("Error");
    }
    OptionalInt min = table.stream().mapToInt(Team::getWins).min();
    if (min.isPresent()) {
      System.out.printf("The'Lowest Number of Loss is %d\n", min.getAsInt());
    } else {
      System.out.println("Error");
    }
    Integer ans = table.stream().map(Team::getRunsScored).reduce(0, (a, b) -> a + b);
    System.out.println("The Total Number of Runs Scored is: " + ans);
    List<String> str = table.stream()
    .filter(p -> p.getRuns_Allowed() < 7)
    .map(Team::getTeam_Name)
    .collect(Collectors.toList());
    System.out.println("Teams which have more than 7 wins are : " + str.toString());
  }
}
