import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class playerNameTest {

	@Test
	public void testPlayerName() {
        Abominodo game = new Abominodo();
        game.start();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        game.data.playerName = "Sri Krishortan";
        game.promptPlayerName();
        assertEquals("Hello Sri Krishortan\n", outContent.toString());
    }
}
