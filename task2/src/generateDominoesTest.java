import static org.junit.Assert.*;

import org.junit.Test;

public class generateDominoesTest {

	@Test
	public void testGenerateDominoes() {
	    Domino_g game = new Domino_g();
        game.generateDominoes();
        assertEquals(28, game._d.size());
	}
}
