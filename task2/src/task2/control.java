package task2;

public class control {
	private player model;
	private menu view;
	
	public control (player model, menu view) {
		this.model = model;
		this.view = view;
	}
	
	public void setname(String name) {
		model.setName(name);
	}
	
	public String getname() {
		return model.getName();
	}
	
	public void updateView() {
		view.mainGUI();

}
}