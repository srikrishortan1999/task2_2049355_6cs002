package task2;
import java.util.ArrayList;
import java.util.List;

public class subject {
	private List <Observer> observers = new ArrayList <Observer>();
	private int s;

	   public int getState() {
	      return s;
	   }

	   public void setState(int s) {
	      this.s = s;
	      notifyAllObservers();
	   }

	public void attach(Observer observe){
	      observers.add(observe);		
	   }

	   public void notifyAllObservers(){
	      for (Observer observe : observers) {
	         observer.update();
	      }
	   } 	
}

	

	   
