import java.awt.Color;
import java.awt.Graphics;

public class DrawDigitGivenCentreParameter {
	public Graphics g;
	public int x;
	public int y;
	public int diameter;
	public int n;
	public Color c;

	public DrawDigitGivenCentreParameter(Graphics g, int x, int y, int diameter, int n, Color c) {
		this.g = g;
		this.x = x;
		this.y = y;
		this.diameter = diameter;
		this.n = n;
		this.c = c;
	}
}