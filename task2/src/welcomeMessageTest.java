import static org.junit.Assert.*;

import org.junit.Test;

public class welcomeMessageTest {

	@Test
	public void testWelcomeMessage() {
        Abominodo game = new Abominodo();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        game.displayWelcomeMessage();
        assertEquals("Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe\n", 
                      outContent.toString());
    }
}
