package task4;
import java.lang.reflect.Field;

public class reflection_7 {
	public static void main(String[] args) throws Exception {
	    main_reflection Result = new main_reflection();
	    Field[] fields = Result.getClass().getDeclaredFields();
	    System.out.printf("There are %d fields\n", fields.length);
	    for (Field f : fields) {
	      f.setAccessible(true);
	      System.out.printf("The Field name is %s, The Type is %s, The Value is %.2f\n", f.getName(),
	          f.getType(), f.getFloat(Result));
	    }
	  }
}
