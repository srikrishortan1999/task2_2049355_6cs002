package task4;

public class main_reflection {

	  public float x = 50.25f;
	  private  float y = 20.78f;

	  public main_reflection() {
	  }

	  public main_reflection( float x, float y) {
	    this.x = x;
	    this.y = y;
	  }

	  public void squareX() {
	    this.x *= this.x;
	  }

	  private void squareY() {
	    this.y *= this.y;
	  }

	  public  float getA() {
	    return x;
	  }

	  private void setX(float x) {
	    this.x = x;
	  }

	  public  float getY() {
	    return y;
	  }

	  public void setY(float y) {
	    this.y = y;
	  }

	  public String toString() {
	    return String.format("(x : %.2f , y : %.2f)", x, y);
	  }
	}

