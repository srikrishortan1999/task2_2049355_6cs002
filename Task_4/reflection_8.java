package task4;
import java.lang.reflect.*;

public class reflection_8 {
	public static void main(String[] args) throws Exception {
	    main_reflection Result = new main_reflection();
	    Field[] fields = Result.getClass().getDeclaredFields();
	    System.out.printf("There are %d fields\n", fields.length);
	    for (Field f : fields) {
	      f.setAccessible(true);
	      float x = f.getFloat(Result);
	      x++;
	      f.setFloat(Result, x);
	      System.out.printf("The Field name is %s, The Type is %s, The Value is %.2f\n", f.getName(),
	          f.getType(), f.getFloat(Result));
	    }
	  }

}
