package task4;
import java.lang.reflect.Field;

public class reflection_6 {
	 public static void main(String[] args) throws Exception {
		    main_reflection Result = new main_reflection ();
		    Field[] fields = Result.getClass().getDeclaredFields();
		    System.out.printf("There are %d fields\n", fields.length);
		    for (Field f : fields) {
		      System.out.printf("The Field Name is %s, The Type is %s, Accessiblity is %s\n", f.getName(),
		          f.getType(), f.isAccessible());
		    }
		  }
}
