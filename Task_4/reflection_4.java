package task4;
import java.lang.reflect.Field;

public class reflection_4 {
	public static void main(String[] args) throws Exception {
	    main_reflection Result = new main_reflection();
	    Field[] fields =Result.getClass().getFields();
	    System.out.printf("There is %d field\n", fields.length);
	    for (Field f : fields) {
	      System.out.printf("The Field Name %s\nThe Type is %s \nThe Value is %.2f\n", f.getName(),
	          f.getType(), f.getFloat(Result));
	    }
	  }
}
