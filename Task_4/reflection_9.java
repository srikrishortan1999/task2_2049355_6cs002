package task4;
import java.lang.reflect.Method;

public class reflection_9 {
	public static void main(String[] args) throws Exception {
	    main_reflection Result = new main_reflection();
	    Method[] methods = Result.getClass().getMethods();
	    System.out.printf("There are %d methods\n", methods.length);

	    for (Method m : methods) {
	      System.out.printf("The Method Name = %s, The Type = %s, The Parameters = ", m.getName(),
	          m.getReturnType());
	      Class[] types = m.getParameterTypes();
	      for (Class c : types) {
	        System.out.print(c.getName() + " ");
	      }
	      System.out.println();
	    }
	  }
}
