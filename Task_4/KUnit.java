package kunit2_task4;
import java.util.*;

public class KUnit {
  private static List<String> checks;
  private static int no_of_checks = 0;
  private static int checks_passed = 0;
  private static int checks_failed = 0;
  
  private static void addToReport(String z) {
	    if (checks == null) {
	      checks = new LinkedList<String>();
	    }
	    checks.add(String.format("%04d: %s", no_of_checks++, z));
	  }
  
  public static void checkEquals(float x, float y) {
	    if (x == y) {
	      addToReport(String.format("  %.2f == %.2f", x, y));
	      checks_passed++;
	    } else {
	      addToReport(String.format("* %.2f == %.2f", x, y));
	      checks_failed++;
	    }
	  }
  
  public static void checkNotEquals(float x, float y) {
	    if (x != y) {
	      addToReport(String.format("  %.2f != %.2f", x,y));
	      checks_passed++;
	    } else {
	      addToReport(String.format("* %.2f != %.2f", x,y));
	      checks_failed++;
	    }
	  }

  public static void report() {
	System.out.printf("Check Results\n");
	System.out.printf("---------------------------\n");
    System.out.printf("No of checks passed = %d\n", checks_passed);
    System.out.printf("No of checks failed = %d\n", checks_failed);
	System.out.printf("---------------------------\n");
    System.out.println();   
    for (String check : checks) {
      System.out.println(check);
    }
  }
}
