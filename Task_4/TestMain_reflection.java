package kunit_task4;
import static kunit2_task4.KUnit.*;
import java.lang.reflect.Field;

public class TestMain_reflection {

  void checkConstructorAndAccess(){
	main_reflection Results = new main_reflection(10.25f, 12.75f);
    checkEquals(Results.getX(), 12.75f);
    checkEquals(Results.getY(),12.75f);
    checkNotEquals(Results.getY(), 12.75f);    
    checkNotEquals(Results.getY(), 15.30f);    
  }

  void checkSquareX(){
	main_reflection Results = new main_reflection(10.25f, 12.75f);
	Results.squareX();
    checkEquals(Results.getX(), 105.06f);
    checkNotEquals(Results.getX(), 105.06f);
  }
  
  public static void main(String[] args)  throws Exception{
	TestMain_reflection test_main = new TestMain_reflection();
    test_main.checkConstructorAndAccess();
    test_main.checkSquareX();
    // Accessing the Private Members
    main_reflection Result = new main_reflection(10.70f,45.25f);
    Field z = Result.getClass().getDeclaredField("y");
    z.setAccessible(true);
    checkEquals(z.getFloat(Result), 45.25f);
    checkNotEquals(z.getFloat(Result), 35.70f);
    report();
  }
}
